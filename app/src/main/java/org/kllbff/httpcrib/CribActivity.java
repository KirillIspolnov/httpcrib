package org.kllbff.httpcrib;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class CribActivity extends AppCompatActivity implements Constants {
    private WebView webView;
    private CribReader cribs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crib);

        webView = findViewById(R.id.document_view);
        webView.setHorizontalScrollBarEnabled(false);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cribs = CribReader.getInstance();

        Intent intent = getIntent();
        if(intent != null) {
            String theme = intent.getStringExtra(THEME_KEY);
            showCrib(theme);
        }
    }

    private void showCrib(String theme) {
        try {
            String fileName = cribs.getFileName(theme);
            webView.loadUrl("file:///android_asset/cribs/" + fileName);
        } catch(Throwable t) {
            Toast.makeText(this, t.toString(), Toast.LENGTH_LONG).show();
        }
    }
}

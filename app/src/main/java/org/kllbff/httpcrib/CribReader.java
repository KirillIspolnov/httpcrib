package org.kllbff.httpcrib;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CribReader {
    private static final String TAG_NAME = CribReader.class.getSimpleName();

    private static CribReader instance;

    public static CribReader getInstance() {
        return instance;
    }

    public static void newInstance(Context ctx) {
        instance = new CribReader(ctx);
    }

    private List<String> themes, files;
    private List<Drawable> icons;

    private CribReader(Context ctx) {
        Resources res = ctx.getResources();
        themes = Collections.unmodifiableList(Arrays.asList(res.getStringArray(R.array.themes)));
        files = Arrays.asList(res.getStringArray(R.array.files));
        icons = new ArrayList<>();

        TypedArray iconsIdsArray = res.obtainTypedArray(R.array.icons);
        for(int i = 0; i < iconsIdsArray.length(); i++) {
            icons.add(iconsIdsArray.getDrawable(i));
        }
        iconsIdsArray.recycle();
    }

    public List<String> getThemes() {
        return themes;
    }

    public List<Drawable> getIcons() {
        return icons;
    }

    public String getFileName(String theme) {
        int index = themes.indexOf(theme);
        if(index == -1) {
            return null;
        }
        return files.get(index);
    }

    private void tryClose(InputStream inputStream) throws IOException {
        try {
            inputStream.close();
        } catch(Throwable t) {
            Log.e(TAG_NAME, "Failed to close input stream", t);
            throw t; //throw up
        }
    }
}

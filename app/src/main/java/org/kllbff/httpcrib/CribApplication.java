package org.kllbff.httpcrib;

import android.app.Application;

public class CribApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        CribReader.newInstance(this);
    }
}

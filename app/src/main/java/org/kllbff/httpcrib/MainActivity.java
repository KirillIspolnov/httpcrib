package org.kllbff.httpcrib;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.List;

public class MainActivity extends AppCompatActivity implements ListView.OnItemClickListener, Constants {
    private ListView listView;
    private CribReader cribs;
    private List<String> themesList;
    private List<Drawable> iconsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView = findViewById(R.id.themes_view);
        cribs = CribReader.getInstance();

        themesList = cribs.getThemes();
        iconsList = cribs.getIcons();

        ThemesAdapter adapter = new ThemesAdapter(this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        listView.setOnItemClickListener(null);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int index, long id) {
        Intent intent = new Intent(this, CribActivity.class);
        intent.putExtra(THEME_KEY, themesList.get(index));
        startActivity(intent);
    }

    public class ThemesAdapter extends ArrayAdapter<String> {
        private LayoutInflater inflater;

        public ThemesAdapter(@NonNull Context context) {
            super(context, android.R.layout.simple_list_item_1);
            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return themesList.size();
        }

        @Override
        public String getItem(int position) {
            return themesList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            View listItemRoot = inflater.inflate(R.layout.list_item, parent, false);

            TextView themeView = listItemRoot.findViewById(R.id.list_item_theme);
            ImageView iconView = listItemRoot.findViewById(R.id.list_item_icon);

            themeView.setText(themesList.get(position));
            iconView.setImageDrawable(iconsList.get(position));

            return listItemRoot;
        }
    }
}
